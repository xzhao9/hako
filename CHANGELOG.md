# Change Log

## v0.8.5

**Implement Enhancements:**

- [Tasks] Add task hierarchies. Support up to 5 level subtasks.
  * Support move up, down, left and right of the tasks.
  * Add actions to the buttons and keyboard shortcuts.

## v0.8.1

**Implement Enhancements:**

- [Task Canvas] Add auto-save feature.
- [Task Canvas] Add task list feature.

**BugFix:**

- [Task Canvas] Minor bug fixes on auto-save and task-list.

## v0.8

**Implement Enhancements:**

- [Task Canvas] first usable version.
