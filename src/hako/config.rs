extern crate dirs;
extern crate toml;

use std::fs;
use std::path::PathBuf;
use toml::Value;

pub fn app_dir() -> PathBuf {
  match dirs::home_dir() {
    Some(s) => {
      s.join(".hako")
    },
    None => {
      panic!("Sorry, I cannot find your home folder.");
    }
  }
}

pub fn config() -> Value {
  let fp = app_dir().join("config.toml");
  let config = fs::read_to_string(fp).unwrap();
  let value = config.parse::<Value>().unwrap();
  value
}
