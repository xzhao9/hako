// Author: Xu Zhao (i@xuzhao.net)
// Date: Dec 31, 2018

//! Hako main module file
//! Defines the Hako interfaces

extern crate openssl;

pub mod config;
pub mod crypt;
pub mod tasks;
pub mod interaction;
pub mod error;
pub mod daemon;
pub mod vault;
pub mod asset;

use std::fs;
use std::path::PathBuf;
use std::os::unix::fs::PermissionsExt;
use openssl::pkey;
use openssl::rsa::{Rsa};

const PUBLICKEYNAME: &str = "hako.publickey";
const PRIVATEKEYNAME: &str = "hako.privatekey";
const OVERWRITEMSG: &str = "Public/Private key already exists. Overwrite? [y/N] ";

pub struct Hako {
  app_path: PathBuf,
  public_key: Option<Vec<u8>>,
  private_key: Option<Vec<u8>>,
}

impl Hako {
  pub fn init(&mut self) -> std::io::Result<()> {
    if !self.app_path.exists() || self.app_path.is_file() {
      if self.app_path.is_file() {
        fs::remove_file(&self.app_path)?;
      }
      // NOTE Create a dir with specified permissions is not implemented.
      // See: https://github.com/rust-lang/rfcs/issues/939
      fs::create_dir(&self.app_path)?;
      Hako::check_reset_perm_mode(&self.app_path, 0o700)?;
    } else {
      Hako::check_reset_perm_mode(&self.app_path, 0o700)?;
      // Load the user public key and private key, if they exist
      let pubkey_path = self.app_path.join(PUBLICKEYNAME);
      let privkey_path = self.app_path.join(PRIVATEKEYNAME);
      if pubkey_path.exists() && privkey_path.exists() {
        Hako::check_reset_perm_mode(&pubkey_path, 0o444)?;
        self.public_key = Some(fs::read(pubkey_path)?);
        Hako::check_reset_perm_mode(&privkey_path, 0o400)?;
        self.private_key = Some(fs::read(privkey_path)?);
      }
    }
    Ok(())
  }
  
  pub fn create() -> Hako {
    Hako {
      app_path: config::app_dir(),
      public_key: None,
      private_key: None,
    }
  }
 
  // =================== Key Operations ====================
  pub fn keygen(&self) {
    if self.private_key != None || self.public_key !=  None {
      if !interaction::yes_or_no(OVERWRITEMSG) {
        println!("The existing keys are not modified.");
        return;
      }
      self.remove_keys().expect("Can't remove old key pairs, please check permissions.");
    }
    let rsa = Rsa::generate(2048).unwrap();
    let passphrase = match interaction::confirm_passphrase(true) {
      Some(s) => s,
      None => { println!("Aborted. Existing keys are not modified."); return; }
    };
    
    let private_key = rsa.private_key_to_pem_passphrase(openssl::symm::Cipher::chacha20(),
                                                        &passphrase.into_bytes()).expect("Can't generate pem for private key.");
    let public_key = rsa.public_key_to_pem().expect("Can't generate pem for public key.");
    self.save_keys(public_key, private_key).expect("Can't save new key contents, please check permissions");
  }
  
  pub fn remove_keys(&self) -> std::io::Result<()> {
    let pubkey_path = self.app_path.join(PUBLICKEYNAME);
    let prikey_path = self.app_path.join(PRIVATEKEYNAME);
    fs::remove_file(pubkey_path)?;
    fs::remove_file(prikey_path)?;
    Ok(())
  }
  
  fn save_keys(&self, pubkey: Vec<u8>, privkey: Vec<u8>) -> std::io::Result<()> {
    let pubkey_path = self.app_path.join(PUBLICKEYNAME);
    let privkey_path = self.app_path.join(PRIVATEKEYNAME);
    Hako::write_u8_to_file_create(&pubkey_path, pubkey)?;
    Hako::check_reset_perm_mode(&pubkey_path, 0o444)?;
    println!("Written public key: {}", pubkey_path.to_str().unwrap());
    Hako::write_u8_to_file_create(&privkey_path, privkey)?;   
    Hako::check_reset_perm_mode(&privkey_path, 0o400)?;
    println!("Written private key: {}", privkey_path.to_str().unwrap());
    Ok(())
  }
  
  pub fn verify_public_key(&self) -> Result<Rsa<pkey::Public>, error::KeyError> {
    if self.public_key == None {
      return Err(error::KeyError::Missing);
    }
    Ok(Rsa::public_key_from_pem(self.public_key.as_ref().unwrap()).unwrap())
  }
  
  pub fn verify_private_key(&self, pass: Option<String>) -> Result<(Rsa<pkey::Private>, String), error::KeyError> {
    if self.private_key == None {
      return Err(error::KeyError::Missing);
    }
    let passphrase = match pass {
      Some(x) => x,
      None => {
        match interaction::confirm_passphrase(false) {
          Some(s) => s,
          None => { println!("Aborted."); return Err(error::KeyError::PassphraseError); }
        }
      }
    };
    Ok((Rsa::private_key_from_pem_passphrase(self.private_key.as_ref().unwrap(),
                                            passphrase.as_bytes()).unwrap(), passphrase))
  }

  // =================== Helpers ====================
  fn write_u8_to_file_create(path: &PathBuf, val: Vec<u8>) -> std::io::Result<()> {
    assert_eq!(path.exists(), false);
    fs::write(&path, val)?;
    Ok(())
  }
  fn check_reset_perm_mode(path: &PathBuf, mode: u32) -> std::io::Result<()> {
    let perm = fs::metadata(path)?.permissions();
    let desired_perm = PermissionsExt::from_mode(mode);
    if perm != desired_perm {
      fs::set_permissions(path, desired_perm)?;
    }
    Ok(())
  }
}
