// Author: Xu Zhao (i@xuzhao.net)
// Date: Jan  1, 2019

//! Error types

use std::error;
use std::fmt;

#[derive(Debug)]
pub enum KeyError {
  Missing,
  NoPermission,
  PassphraseError,
}

#[derive(Debug)]
pub struct AuthError {
  details: String,
}

impl AuthError {
  pub fn new(msg: &str) -> AuthError {
    AuthError{details: msg.to_owned()}
  }
}

impl fmt::Display for AuthError {
  fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
    write!(f, "{}", self.details)
  }
}

impl error::Error for AuthError {
  fn description(&self) -> &str {
    &self.details
  }
}

impl fmt::Display for KeyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
          KeyError::Missing =>
            write!(f, "Please ensure both of public and private key files exist."),
          KeyError::NoPermission =>
            write!(f, "Please check proper permissions are set to the key files."),
          KeyError::PassphraseError => 
            write!(f, "Please enter the correct passphrase."),
        }
    }
}

impl error::Error for KeyError {
  fn description(&self) -> &str {
    match *self {
      KeyError::Missing => "One or two key files are missing.",
      KeyError::NoPermission => "I don't have read permission to the key files.",
      KeyError::PassphraseError => "I can't decrypt the private key with the provided passphrase.",
    }
  }
  fn cause(&self) -> Option<&error::Error> {
    match *self {
      KeyError::Missing => None,
      KeyError::NoPermission => None,
      KeyError::PassphraseError => None,
    }
  }
}

