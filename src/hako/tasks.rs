// Author: Xu Zhao (i@xuzhao.net)
// Date: Nov 30, 2019

//! Helper functions for Hako GTK feature
extern crate serde;
extern crate serde_json;

use std::io;
use std::fs;
use std::path::PathBuf;
use serde::{Deserialize, Serialize};
use serde_json::Result;

const TITLE_PREFIX: &str = "#+TITLE:";

#[derive(Serialize, Deserialize)]
pub struct TaskFileMeta {
  pub name: String,
  pub title: String,
}

#[derive(Serialize, Deserialize)]
pub struct TaskList {
  pub tasks: Vec<TaskFileMeta>
}

#[derive(Serialize, Deserialize)]
pub struct Task {
  pub text: String,
  pub todo: bool,
  pub level: i32,
}

#[derive(Serialize, Deserialize)]
pub struct TaskFile {
  pub meta: TaskFileMeta,
  pub tasks: Vec<Task>,
}

// append the name to the end of task_dir
pub fn concat_dir_name(task_dir: &PathBuf, name: &str) -> String {
  let mut fulldir = task_dir.clone();
  fulldir.push(name);
  fulldir.to_string_lossy().to_string()
}

pub fn read_file_to_string(path: String) -> std::io::Result<String> {
  return fs::read_to_string(path);
}

pub fn write_string_to_file(path: String, content: String) -> std::io::Result<()> {
  fs::write(path, content)
}

// Serialize is not used but we keep it for the record
#[allow(dead_code)]
pub fn serialize<T: Serialize>(obj: T) -> String {
  serde_json::to_string(&obj).ok().unwrap()
}

pub fn deserialize<'a, T: Deserialize<'a>>(json: &'a str) -> Result<T> {
  serde_json::from_str(json)
}

pub fn org_to_taskfile(meta: TaskFileMeta, org: String) -> TaskFile {
  let mut ret = TaskFile {
    meta: meta,
    tasks: Vec::new(),
  };
  // split org to lines
  for line in org.lines() {
    match get_task(line) {
      Some(t) => {
        ret.tasks.push(t);
      },
      None => { },
    }
  }
  ret
}

pub fn taskfile_to_org(tfile: &TaskFile) -> String {
  let mut ret = format!("#+TITLE: {}\n\n", &tfile.meta.title);
  for task in &tfile.tasks {
    // #prefix #todo
    let prefix = String::from_utf8(vec![b'*'; task.level as usize]).unwrap();
    let todo = match task.todo { true => "TODO", false => "DONE" };
    let line = format!("{} {} {}\n", prefix, todo, task.text);
    ret.push_str(&line);
  }
  ret
}

pub fn get_task(line: &str) -> Option<Task> {
  let mut task: Option<Task> = None;
  let mut index = 0; // length of the prefix '*'
  loop {
    if line.len() > index && line.chars().nth(index).unwrap() == '*' {
      index += 1;
    } else {
      break;
    }
  }
  if index != 0 && (index+5) < line.len() {
    if line.chars().nth(index).unwrap() == ' ' && line.chars().nth(index+5).unwrap() == ' ' {
      let todo: bool;
      let withtodo: bool;
      let status = &line[index+1..index+5];
      if status == "TODO" {
        todo = true;
        withtodo = true;
      } else if status == "DONE" {
        todo = false;
        withtodo = true;
      } else { // Not mentioned, treat as "TODO"
        todo = true;
        withtodo = false;
      }
      let content = match withtodo {
        true => line[index+5..].trim().to_string(),
        false => line[index+1..].trim().to_string()
      };
      task = Some(Task {
        text: content,
        todo: todo,
        level: index as i32,
      });
    }
  }
  task
}

pub fn get_title(first_line: String) -> Option<String> {
  if first_line.starts_with(TITLE_PREFIX) {
    let title = format!("{}", first_line[TITLE_PREFIX.len()..].trim());
    if title.len() != 0 {
      Some(title)
    } else {
      None
    }
  } else {
    return None;
  }
}

