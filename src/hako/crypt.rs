// Author: Xu Zhao (i@xuzhao.net)
// Date: Dec 31, 2018

//! Main function of Hako encryption/decryption
extern crate openssl;
extern crate rand;
extern crate crypto;

use super::Hako;
use std::path::Path;
use std::path::PathBuf;
use std::io::ErrorKind;
use rand::rngs::OsRng;
use rand::RngCore;
use openssl::pkey;
use openssl::rsa::{Padding, Rsa};
use crypto::chacha20poly1305::ChaCha20Poly1305;
use crypto::aead::{AeadEncryptor, AeadDecryptor};
 
pub fn encrypt(hako: Hako, fname: &String) -> std::io::Result<()> {
  let path = Path::new(fname);
  if !path.exists() {
    return Err(std::io::Error::new(ErrorKind::NotFound, "The specified file does not exist."));
  }
  if !path.is_file() {
    return Err(std::io::Error::new(ErrorKind::NotFound, "The specified file is not a regular file. May be a directory?"));
  }
  let message = std::fs::read(path)?;
  return encrypt_string(&hako, &PathBuf::from(format!("{}.hako", fname)), message.as_slice());
}

// Encrypt plaintext content and save to file with fname
pub fn encrypt_string(hako: &Hako, fname: &PathBuf, message: &[u8]) -> std::io::Result<()> {
  let public = match hako.verify_public_key() {
    Ok(s) => s,
    Err(err) => {
      println!("{}", err);
      return Ok(());
    }
  };
  
  let mut symmetric_key = [0u8; 32];
  OsRng.fill_bytes(&mut symmetric_key[..]);
  let mut encrypted_symmetric_key = [0u8; 256];
  public.public_encrypt(&symmetric_key,
                        &mut encrypted_symmetric_key, Padding::PKCS1).unwrap();
  let mut c = ChaCha20Poly1305::new(&symmetric_key, &[0u8; 8][..], &[]);
  let mut output = vec![0; 256 + 16 + message.len()];
  let mut tag = [0u8; 16];
  c.encrypt(&message, &mut output[256+16..], &mut tag[..]);
  for i in 0..256 {
    output[i] = encrypted_symmetric_key[i];
  }
  for i in 0..16 {
    output[256+i] = tag[i];
  }
  std::fs::write(&fname, output)?;
  Ok(())
}

pub fn decrypt(secret: &Rsa<pkey::Private>, fname: &PathBuf) -> std::io::Result<Vec<u8>> {
  if !fname.exists() {
    println!("The specified file does not exist.");
    return  Err(std::io::Error::new(ErrorKind::NotFound, "The specified file does not exist."));
  }
  if !fname.is_file() {
    return Err(std::io::Error::new(ErrorKind::NotFound, "The specified file is not a regular file. May be a directory?"));
  }
  let message: Vec<u8> = std::fs::read(fname).unwrap();
  let encrypted_symmetric_key = &message[0..256];
  let tag = &message[256..256+16];
  let mut symmetric_key = [0u8; 256];
  let keysize = secret.private_decrypt(&encrypted_symmetric_key, &mut symmetric_key, Padding::PKCS1).unwrap();
  let mut d = ChaCha20Poly1305::new(&symmetric_key[0..keysize], &[0u8; 8][..], &[]);
  let mut plaintext = vec![0; message.len() - 256 - 16];
  if !d.decrypt(&message[256+16..], &mut plaintext[..], tag) {
    println!("Fatal Error: Invalid encrypted data!");
    std::process::exit(1);
  }
  Ok(plaintext)
}
