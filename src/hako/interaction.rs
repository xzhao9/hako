// Author: Xu Zhao (i@xuzhao.net)
// Date: Dec 31, 2018

//! User interactions
extern crate termion;

use termion::input::TermRead;
use std::io;
use std::io::{stdin, stdout};
use std::io::Write;

pub fn confirm_passphrase(repeat: bool) -> Option<String> {
  let pass = match get_secret("Enter passphrase:") {
    Some(p) => p,
    None => {
      return None;
    }
  };

  if repeat {
    let pass_repeat = match get_secret("Enter the same passphrase again:") {
      Some(p) => p,
      None => {
        return None;
      }
    };
    
    if pass.as_bytes() == pass_repeat.as_bytes() {
      return Some(pass);
    } else {
      println!("Passphrases do not match. Try again.");
      return confirm_passphrase(repeat);
    }
  } else {
    Some(pass)
  }
}

fn get_secret(msg: &str) -> Option<String> {
  let stdout = stdout();
  let mut stdout = stdout.lock();
  stdout.write_all(msg.as_bytes()).unwrap();
  io::stdout().flush().expect("Fatal: can't flush query message to stdout.");
  let stdin = stdin();
  let mut stdin = stdin.lock();
  let pass = stdin.read_passwd(&mut stdout).unwrap();
  stdout.write_all(b"\n").unwrap();
  return pass;
}

pub fn yes_or_no(msg: &str) -> bool {
  print!("{}", msg);
  io::stdout().flush().expect("Fatal: can't flush query message to stdout.");
  let mut buffer = String::new();
  io::stdin().read_line(&mut buffer).expect("Fatal: can't read your input.");
  match buffer.trim().to_lowercase().as_str() {
    "y" => {
      true
    },
    "n" => {
      false
    },
    "" => {
      false
    },
    _ => { println!("Please answer y or n. "); yes_or_no(msg) }
  }
}

