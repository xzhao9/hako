// Author: Xu Zhao (i@xuzhao.net)
// Date: Jan  5, 2019

//! Hako daemon mode
//! Serves as an encrypted key-value store

extern crate rouille;
extern crate toml;
extern crate openssl;
extern crate base64;
extern crate shellexpand;

use super::Hako;
use super::error::AuthError;
use std::str;
use std::fs;
use std::io::Read;
use std::io::BufReader;
use std::io::BufRead;
use std::path::Path;
use std::ffi::OsStr;
use std::path::PathBuf;
use std::error::Error;
use rouille::Request;
use rouille::{Response};
use toml::Value;

use openssl::pkey;
use openssl::rsa::{Padding, Rsa};

use crypto::salsa20::Salsa20;
use crypto::symmetriccipher::{Encryptor, Decryptor};


const DEFAULT_PORT: &str = "8341";
const ENCRYPTED_SYMKEY_LEN: usize = 256;
const SYMKEY_LEN: usize = 32;
const TAG_LEN: usize = 24;
const SALT_LEN: usize = 16;

pub struct Daemon {
  public: Rsa<pkey::Public>,
  private: Rsa<pkey::Private>,
  passphrase: String,
  hako: Hako,
  vault_file: PathBuf,
  tasks_dir: Option<PathBuf>,
  config: Value,
}

#[derive(Debug)]
pub enum ReqType {
  ADD,
  RM,
  UPDATE,
}

impl Daemon {
  pub fn init() -> std::io::Result<Daemon> {
    let mut hako = Hako::create();
    hako.init()?;
    let config = super::config::config();
    let vault_file = hako.app_path.join(config["vault"].as_str().unwrap());
    let tasks_dir = match config["tasks"].as_str() {
      Some(s) => {
        let pb = PathBuf::from(shellexpand::tilde(s).as_ref());
        if pb.exists() {
          Some(pb)
        } else {
          None
        }
      },
      None => {
        None
      }
    };
    let publickey = hako.verify_public_key().unwrap();
    let (privatekey, passphrase) = hako.verify_private_key(Some(config["password"].as_str().unwrap().to_string())).unwrap();
    Ok(Daemon {
      public: publickey,
      private: privatekey,
      passphrase: passphrase,
      hako: hako,
      vault_file: vault_file,
      tasks_dir: tasks_dir,
      config: config,
    })
  }
  
  pub fn start() {
    let daemon = Daemon::init().unwrap();
    println!("Serving Hako daemon at port: {}", DEFAULT_PORT);
    rouille::start_server(["0.0.0.0:", DEFAULT_PORT].join(""),  move |request| {
      Daemon::handle_request(&daemon, request)
    });
  }

  // Dispatch the request according to the path and session
  fn handle_request(daemon: &Daemon, request: &Request) -> Response {
    let url = request.url();
    match url.as_str() {
      "/getlogin" => {
        Daemon::get_login(daemon, request)
      },
      "/addlogin" => {
        Daemon::set_login(daemon, request, ReqType::ADD)
      },
      "/rmlogin" => {
        Daemon::set_login(daemon, request, ReqType::RM)
      },
      "/updatelogin" => {
        Daemon::set_login(daemon, request, ReqType::UPDATE)
      },
      "/listtasks" => { // List all todo org files
        Daemon::list_tasks(daemon, request)
      },
      "/loadtask" => { // load one specific todo org file
        Daemon::load_task(daemon, request)
      },
      "/savetask" => { // load one specific todo org file
        Daemon::save_task(daemon, request)
      },
      otherurl => {
        Daemon::find_assets(daemon, otherurl, request)
      }
    }
  }

  fn find_assets(daemon: &Daemon, url: &str, _req: &Request) -> Response {
    match super::asset::Asset::get(super::asset::trim_prefix_slash(url)) {
      Some(data) => {
        // get file extension of the url
        let path = Path::new(url);
        let mime = super::asset::extension_to_mime(path.extension().and_then(OsStr::to_str));
        Response::from_data(mime, data)
      },
      None => {
        Daemon::bailout(format!("Unrecognized bad request"))
      }
    }
  }
  
  fn list_tasks(daemon: &Daemon, _req: &Request) -> Response {
    if daemon.tasks_dir == None {
      return Daemon::bailout(format!("Tasks dir is not set. Please specify an existing directory in your config."));
    }
    let mut paths: Vec<_> = fs::read_dir(&daemon.tasks_dir.as_ref().unwrap()).unwrap()
      .map(|r| r.unwrap()).collect();
    let mut tlist = super::tasks::TaskList {
      tasks: Vec::new(),
    };
    paths.sort_by_key(|dir| dir.path());
    // sort the result
    for path in paths {
      let fpath = path.path();
      let file = match fs::File::open(&fpath) {
        Ok(f) => f,
        Err(_) => {
          return Daemon::bailout(format!("Can't read task file folder."));
        }
      };
      let mut first_line = String::new();
      let mut buffer = BufReader::new(file);
      match buffer.read_line(&mut first_line) {
        Ok(_) => { },
        Err(_) => {
          return Daemon::bailout(format!("Failed when reading the first line!"));
        }
      }

      let name = fpath.file_name().unwrap();
      // If the first_line starts with #+TITLE:, read the name of the list
      let title = super::tasks::get_title(first_line);
      if title != None {
        let tmeta = super::tasks::TaskFileMeta {
          name: format!("{}", name.to_str().unwrap()),
          title: title.unwrap()
        };
        tlist.tasks.push(tmeta);
      }
    }
    // check resp size: if zero, fill in the default: {[{filename: todo.org, title: "Todo List"}]}
    if tlist.tasks.len() == 0 {
      let tmeta = super::tasks::TaskFileMeta {
        name: format!("todo.org"),
        title: format!("Todo list"),
      };
      tlist.tasks.push(tmeta);
    }
    
    // Response format: {[{filename: name1, title: title1}, {filename: name2, title: title2}]}
    Response::json(&tlist)
  }
  
  fn save_task(daemon: &Daemon, req: &Request) -> Response {
    if daemon.tasks_dir == None {
      return Daemon::bailout(format!("Task dir is not set. Please specify an existing directory in your config."));
    }
    let mut data = req.data().expect("Failed to read request body.");
    let mut buf: Vec<u8> = Vec::new();
    match data.read_to_end(&mut buf) {
      Ok(_) => {},
      Err(_) => {return Daemon::bailout(format!("Failed to read the request to buffer."));}
    }
    let tasks: super::tasks::TaskFile = match super::tasks::deserialize(str::from_utf8(&buf).unwrap()) {
      Ok(t) => t,
      Err(_) => {
        return Daemon::bailout(format!("Failed to deserialize the task json."));
      }
    };
    let taskorgtext = super::tasks::taskfile_to_org(&tasks);
    let fullpath = super::tasks::concat_dir_name(daemon.tasks_dir.as_ref().unwrap(),
                                                 &tasks.meta.name);
    match super::tasks::write_string_to_file(fullpath, taskorgtext) {
      Ok(_) => {
        Response::text("Success")
      },
      Err(_) => {
        Daemon::bailout(format!("Failed to save task to file."))
      }
    }
  }

  fn load_task(daemon: &Daemon, req: &Request) -> Response {
    if daemon.tasks_dir == None {
      return Daemon::bailout(format!("Task dir is not set. Please specify an existing directory in your config."));
    }
    let mut data = req.data().expect("Failed to read request body.");
    let mut buf: Vec<u8> = Vec::new();
    match data.read_to_end(&mut buf) {
      Ok(_) => {},
      Err(_) => {return Daemon::bailout(format!("Failed to read the request to buffer."));}
    }
    let tfilemeta: super::tasks::TaskFileMeta = match super::tasks::deserialize(str::from_utf8(&buf).unwrap()) {
      Ok(t) => t,
      Err(_) => {
        return Daemon::bailout(format!("Failed to deserialize the LOAD request"));
      }
    };
    let fullpath = super::tasks::concat_dir_name(daemon.tasks_dir.as_ref().unwrap(),
                                                 &tfilemeta.name);
    let ftext = match super::tasks::read_file_to_string(fullpath) {
      Ok(text) => text,
      Err(_) => {
        return Daemon::bailout(format!("Failed to read specified org file content."));
      }
    };
    Response::json(&super::tasks::org_to_taskfile(tfilemeta, ftext))
  }
  
  // Get the request domain
  fn get_login(daemon: &Daemon, req: &Request) -> Response {
    let (symmetric_key, tag, request_body) = match Daemon::auth(daemon, req) {
      Ok(key) => key,
      Err(err) => {
        return Daemon::bailout(format!("Failed authentication: {}", err.description()));
      }
    };
    // if the request is just to test connection
    if request_body == "TestConnection" {
      return Daemon::sendok(symmetric_key, tag, "OK".as_bytes());
    }

    let plaintext = match super::crypt::decrypt(&daemon.private, &daemon.vault_file) {
      Ok(s) => s,
      Err(_) => {
        return Daemon::bailout("Decryption error.".to_owned());
      }
    };
    let plaintext = String::from_utf8_lossy(&plaintext).to_owned().to_string();
    let result = super::vault::get_login(&plaintext, &request_body);
    Daemon::sendok(symmetric_key, tag, result.as_bytes())
  }

  // Give up with an error message
  fn bailout(msg: String) -> Response {
    println!("Unknown request - {}", msg);
    Response::empty_400()
  }
  
  // Set login information:
  // Request body: domain base update? username password
  fn set_login(daemon: &Daemon, req: &Request, req_type: ReqType) -> Response {
    let (symmetric_key, tag, request_body) = match Daemon::auth(daemon, req) {
      Ok(key) => key,
      Err(err) => {
        return Daemon::bailout(format!("Failed authentication: {}", err.description()));
      }
    };
    // println!("ReqType: {:?} RequestBody: {}", req_type, request_body);
    let plaintext = match super::crypt::decrypt(&daemon.private, &daemon.vault_file) {
      Ok(s) => s,
      Err(_) => {
        return Daemon::bailout("Decryption error.".to_owned());
      }
    };
    let plaintext = String::from_utf8_lossy(&plaintext).to_owned().to_string();
    let split = request_body.split(" ").collect::<Vec<&str>>();
    // Sanity check
    match req_type {
      ReqType::UPDATE => {
        // Domain, base, username, password, oldusername
        if split.len() != 5 {
          Daemon::bailout("setlogin err: Invalid addlogin request".to_owned());
        }
      },
      _ => {
        // Domain, base, username, password
        if split.len() != 4 {
          Daemon::bailout("setlogin err: Invalid addlogin request".to_owned());
        }
      }
    }
    let domain = split[0].to_string();
    let base = split[1].to_string();
    let username = split[2].to_string();
    let password = split[3].to_string();
    let opret = match req_type {
      ReqType::ADD => {
        super::vault::add_login(&plaintext, domain, base, username, password)
      },
      ReqType::RM => {
        super::vault::remove_login(&plaintext, domain, username)
      },
      ReqType::UPDATE => {
        let oldusername = split[4].to_string();
        super::vault::update_login(&plaintext, domain, username, password, oldusername)
      }
    };
    match opret {
      Some(updated_vault) => {
        match super::crypt::encrypt_string(&daemon.hako, &daemon.vault_file, updated_vault.as_bytes()) {
          Ok(_) => {
            Daemon::sendok(symmetric_key, tag, "success".as_bytes())
          },
          Err(_) => {
            Daemon::sendok(symmetric_key, tag, "fail-encrypt".as_bytes())
          }
        }
      },
      None => {
        Daemon::sendok(symmetric_key, tag, "fail-vaultop".as_bytes())
      }
    }
  }

  fn sendok(symmetric_key: Vec<u8>, tag: [u8; 24], msg: &[u8]) -> Response {
    let mut c = Salsa20::new_xsalsa20(&symmetric_key, &tag);
    let mut output = vec![0u8; msg.len()];
    let mut readbuffer = crypto::buffer::RefReadBuffer::new(msg);
    let mut writebuffer = crypto::buffer::RefWriteBuffer::new(&mut output);
    // Encrypt the result
    let final_msg = match c.encrypt(&mut readbuffer, &mut writebuffer, false) {
      Ok(_) => {
        output
      },
      Err(_) => {
        return Daemon::bailout(format!("Encrypt response error"));
      }
    };
    let s = base64::encode(&final_msg);
    let mut r = Response::text(s);
    r.headers.push(("Access-Control-Allow-Origin".into(), "*".into()));
    r
  }
  
  // authentication process
  // 1. Decrypt the first SYMKEY_LEN bytes with the private key, find the shared chacha20 key
  // 2. Decrypt with the chacha20 key, parse the message with the following format:
  // | passphrase_size | passphrase | request_body_size | request_body | salt(SALT_LEN) |
  // 3. Return the symmetric key and the request body
  fn auth(daemon: &Daemon, req: &Request) -> Result<(Vec<u8>, [u8; TAG_LEN] , String), AuthError> {
    let mut data = req.data().expect("Body already retrieved, problem in server.");
    let mut buf: Vec<u8> = Vec::new();
    match data.read_to_end(&mut buf) {
      Ok(_) => (),
      Err(_) => {return Err(AuthError::new("Failed to read request body."));}
    };
    let buf = match base64::decode(&buf) {
      Ok(buf) => buf,
      Err(_) => {return Err(AuthError::new("Request Base64 decode error."));}
    };
    
    let encrypted_symmetric_key = &buf[0..ENCRYPTED_SYMKEY_LEN];
    let tag = &buf[ENCRYPTED_SYMKEY_LEN..ENCRYPTED_SYMKEY_LEN+TAG_LEN];
    let body = &buf[ENCRYPTED_SYMKEY_LEN+TAG_LEN..];
    let mut encoded_symmetric_key = [0u8; ENCRYPTED_SYMKEY_LEN];
    let keysize = match daemon.private.private_decrypt(&encrypted_symmetric_key,
                                                       &mut encoded_symmetric_key, Padding::PKCS1) {
      Ok(size) => size,
      Err(e) => { println!("{}", e); return Err(AuthError::new("Can't decrypt the data.")); }
    };
    
    let symmetric_key = match base64::decode(&String::from_utf8_lossy(&encoded_symmetric_key[0..keysize]).to_string()) {
      Ok(buf) => buf,
      Err(_) => {return Err(AuthError::new("Symmetric key Base64 decode error."));}
    };
    if symmetric_key.len() != SYMKEY_LEN {
      return Err(AuthError::new("Incorrect symmetric key size."));
    }
    let mut s = Salsa20::new_xsalsa20(&symmetric_key, tag);
    let mut plaintext = vec![0; buf.len() - ENCRYPTED_SYMKEY_LEN - TAG_LEN];
    let mut readbuf = crypto::buffer::RefReadBuffer::new(body);
    let mut writebuf = crypto::buffer::RefWriteBuffer::new(&mut plaintext);
    match s.decrypt(&mut readbuf, &mut writebuf, false) {
      Ok(_) => { },
      Err(_) => {
        return Err(AuthError::new("Can't decrypt the data."));
      }
    };
    // Byte Order: Big Endian
    let passphrase_size: usize = u32::from_be_bytes([plaintext[0], plaintext[1],
                                                     plaintext[2], plaintext[3]]) as usize;
    let request_body_size: usize = u32::from_be_bytes([plaintext[4+passphrase_size],
                                                       plaintext[5+passphrase_size],
                                                       plaintext[6+passphrase_size],
                                                       plaintext[7+passphrase_size]]) as usize;
    if plaintext.len() != (8 + passphrase_size + request_body_size + SALT_LEN) as usize {
      return Err(AuthError::new("Invalid request length."));
    }
    // Now suppose the data is valid. First, validate the passphrase.
    if passphrase_size == daemon.passphrase.len() {
      for i in 0..passphrase_size {
        if plaintext[4 + i] != daemon.passphrase.as_bytes()[i] {
          return Err(AuthError::new("Incorrect passphrase"));
        }
      }
    } else {
      return Err(AuthError::new("Incorrect passphrase length"));
    }
    let mut tag_copy = [0u8; TAG_LEN];
    tag_copy.copy_from_slice(tag);
    let request_body = String::from_utf8_lossy(&plaintext[8+passphrase_size..8+passphrase_size+request_body_size]).to_string();
    return Ok((symmetric_key, tag_copy, request_body));
  }
}

// Unit Tests
#[test]
fn test_getlogin() {
  let daemon = Daemon::init().unwrap();
  let mut rng = rand::rngs::OsRng::new().unwrap();
  let passphrase = daemon.config["password"].as_str().unwrap();
  let passphrase_size = passphrase.len() as u32;
  let request_body = "onlineservices.greenshield.ca";
  let request_body_size = request_body.len() as u32;
  let mut salt = [0u8; SALT_LEN];
  // Construct the symmetric key
  let mut symmetric_key = [0u8; SYMKEY_LEN];
  let mut tag = [0u8; TAG_LEN];
  rng.fill_bytes(&mut symmetric_key);
  rng.fill_bytes(&mut tag);
  let encoded_symkey = base64::encode(&symmetric_key);
  let mut encrypted_symkey = [0u8; ENCRYPTED_SYMKEY_LEN];
  daemon.public.public_encrypt(&encoded_symkey.as_bytes(),
                               &mut encrypted_symkey, Padding::PKCS1).unwrap();
  rng.fill_bytes(&mut salt);
  let mut message: Vec<u8> = Vec::new();
  message.extend_from_slice(&passphrase_size.to_be_bytes());
  message.extend_from_slice(passphrase.as_bytes());
  message.extend_from_slice(&request_body_size.to_be_bytes());
  message.extend_from_slice(request_body.as_bytes());
  message.extend_from_slice(&salt);
  // encrypt with the symmetric key
  let mut output: Vec<u8> = vec![0u8; message.len()];
  let mut c = Salsa20::new_xsalsa20(&symmetric_key, &tag);
  let mut readbuf = crypto::buffer::RefReadBuffer::new(&mut message);
  let mut writebuf = crypto::buffer::RefWriteBuffer::new(&mut output);
  c.encrypt(&mut readbuf, &mut writebuf, false).unwrap();
  let mut req_data: Vec<u8> = Vec::new();
  req_data.extend_from_slice(&encrypted_symkey);
  req_data.extend_from_slice(&tag);
  req_data.extend_from_slice(&output);
  let final_data = base64::encode(&req_data);
  let req = rouille::Request::fake_http("POST", "/getlogin",
                                        vec![], // header
                                        final_data.into_bytes()); // data
  let resp = Daemon::handle_request(&daemon, &req);
  let (mut resp_data, size) = resp.data.into_reader_and_size();
  let mut encoded_buf = vec![];
  resp_data.read_to_end(&mut encoded_buf).unwrap();
  assert_eq!(encoded_buf.len(), size.unwrap());
  let _ss = String::from_utf8_lossy(&encoded_buf);

  let buf = base64::decode(&encoded_buf).unwrap();
  let mut plainresp = vec![0u8; buf.len()];
  let mut readbuf = crypto::buffer::RefReadBuffer::new(&buf);
  let mut writebuf = crypto::buffer::RefWriteBuffer::new(&mut plainresp);
  let mut c = Salsa20::new_xsalsa20(&symmetric_key, &tag);
  c.decrypt(&mut readbuf, &mut writebuf, false).unwrap();
  let plain_content = String::from_utf8_lossy(&plainresp).to_owned();
  assert_eq!(plain_content.contains("xz"), true);
}
