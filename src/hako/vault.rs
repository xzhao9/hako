// Author: Xu Zhao (i@xuzhao.net)
// Date: Jan 11, 2019

//! Module dealing with the decrypted vault file

pub struct Login {
  username: String,
  password: String,
}

fn serialize_logins(logins: Vec<Login>) -> String {
  logins.into_iter().map(|x| format!("{} {}", x.username, x.password))
    .collect::<Vec<String>>().join("\n")
}

fn serialize_logins_with_domain(logins: Vec<Login>, hako_domain: String,
                           base_domain: String) -> String {
  let l = logins.into_iter().map(|x| format!("{} {}", x.username, x.password))
    .collect::<Vec<String>>().join(" ");
  format!("{} {} {}\n", hako_domain, base_domain, l)
}

fn get_line(vault_text: &String, linenum: Option<usize>) -> Option<&str> {
  match linenum {
    Some(lnum) => {
      let mut lines = vault_text.lines();
      return Some(lines.nth(lnum as usize).expect("No lines found at specified position."));
    },
    None => {
      return None;
    }
  }
}

fn replace_line(vault_text: &String, linenum: usize, newline: String) -> String {
  let lines = vault_text.lines();
  let mut new_vault:Vec<String> = Vec::new();
  for (i, line) in lines.enumerate() {
    if i == linenum {
      new_vault.push(newline.clone());
    } else {
      new_vault.push(format!("{}\n", line));
    }
  }
  return new_vault.join("");
}

fn append_line(vault_text: &String, new_entry: String) -> String {
  let new_vault_text = format!("{}{}",vault_text, new_entry);
  return new_vault_text;
}

fn remove_line(vault_text: &String, linenum: usize) -> String {
  let lines = vault_text.lines();
  let mut new_vault:Vec<String> = Vec::new();
  for (i, line) in lines.enumerate() {
    if i == linenum {
      // pass
    } else {
      new_vault.push(format!("{}\n", line));
    }
  }
  return new_vault.join("");
}

// return the line number
fn find_site_linenum(vault_text: &String, domain: &String) -> Option<usize> {
  let lines = vault_text.lines();
  let mut base_match: Option<usize> = None;
  for (lnum, line) in lines.enumerate() {
    let fields = line.split(" ").collect::<Vec<&str>>();
    let fdomain = fields[0];
    let fbase = fields[1];
    if fdomain == domain {
      // found the exact matching domain, no need to search further
      return Some(lnum);
    } else if domain.ends_with(fbase) {
      base_match = Some(lnum);
    }
  }
  return base_match;
}

fn logins_from_string(line: &str) -> (String, String, Vec<Login>) {
  let fields = line.split(" ").collect::<Vec<&str>>();
  let mut m: Vec<Login> = Vec::new();
  if fields.len() < 2 {
    panic!("The length of a line must be larger or equal to 2!");
  }
  if fields.len() == 2 {
    return (fields[0].to_string(), fields[1].to_string(), m)
  }
  for x in (2..fields.len()).step_by(2) {
    m.push(Login {
      username: fields[x].to_owned(),
      password: fields[x+1].to_owned(),
    });
  }
  (fields[0].to_string(), fields[1].to_string(), m)
}

fn remove_login_from_logins(logins: Vec<Login>, username: String) -> Vec<Login> {
  let mut updated_logins: Vec<Login> = Vec::new();
  for login in logins {
    if login.username == username {
       // pass 
    } else {
      updated_logins.push(login);
    }
  }
  return updated_logins;
}

fn update_login_to_logins(logins: Vec<Login>, oldusername: String,
                          newusername: String, password: String) -> Option<Vec<Login>> {
  let mut updated_logins: Vec<Login> = Vec::new();
  let mut find = false;
  for login in logins {
    if login.username == oldusername {
      updated_logins.push(Login{username: newusername.clone(), password: password.clone()});
      find = true;
    } else {
      updated_logins.push(login);
    }
  }
  if find {
    Some(updated_logins)
  } else {
    None
  }
}

fn add_login_to_logins(logins: Vec<Login>, username: String, password: String) -> Vec<Login> {
  let mut updated_logins: Vec<Login> = Vec::new();
  for login in logins {
    updated_logins.push(login);
  }
  updated_logins.push(Login{username: username, password: password});
  return updated_logins;
}

pub fn get_login(vault_text: &String, domain: &String) -> String {
  let linenum = find_site_linenum(vault_text, domain);
  let linestr = get_line(vault_text, linenum);
  match linestr {
    None => String::new(), // If failed, return empty string
    Some(lstr) => {
      serialize_logins(logins_from_string(lstr).2)
    }
  }
}

// If successful, return Some(updated_text), else not found, return None
pub fn remove_login(vault_text: &String, domain: String, username: String) -> Option<String> {
  let linenum = find_site_linenum(vault_text, &domain);
  let line = get_line(vault_text, linenum);
  match line {
    Some(line) => {
      let (hako_domain, base_domain, logins) = logins_from_string(line);
      let lnum = linenum.unwrap();
      if logins.len() == 1 { // only one login, remove the entire line
        return Some(remove_line(vault_text, lnum));
      } else { // remove one login
        let updated_logins = remove_login_from_logins(logins, username);
        let updated_text = replace_line(vault_text, lnum,
                                        serialize_logins_with_domain(updated_logins, hako_domain, base_domain));
        return Some(updated_text);
      }
    },
    None => {
      return None;
    }
  }
}

// If successful, return Some(updated_text), else not found, return None
pub fn add_login(vault_text: &String, domain: String, domain_base: String,
                 username: String, password: String) -> Option<String> {
  let linenum = find_site_linenum(vault_text, &domain);
  let line = get_line(vault_text, linenum);
  match line {
    Some(line) => {
      // this login already exists in the vault
      let (hako_domain, base_domain, logins) = logins_from_string(line);
      let lnum = linenum.unwrap();
      let updated_logins = add_login_to_logins(logins, username, password);
      let serialized = serialize_logins_with_domain(updated_logins, hako_domain, base_domain);
      let updated_text = replace_line(vault_text, lnum, serialized);
      return Some(updated_text);
    },
    None => {
      // Add a new domain entry
      let new_entry = format!("{} {} {} {}\n", domain, domain_base, username, password);
      let updated_text = append_line(vault_text, new_entry);
      return Some(updated_text);
    }
  }
}

// If successful, return Some(updated_text), else not found, return None
pub fn update_login(vault_text: &String, domain: String, username: String, password: String, oldusername: String) -> Option<String> {
  let linenum = find_site_linenum(vault_text, &domain);
  let line = get_line(vault_text, linenum);
  match line {
    Some(line) => {
      // this login already exists in the vault
      let (hakodomain, basedomain, logins) = logins_from_string(line);
      let lnum = linenum.unwrap();
      // find the login with oldusername
      match update_login_to_logins(logins, oldusername, username, password) {
        Some(updated_logins) => {
          let updated_text = replace_line(vault_text, lnum,
                                          serialize_logins_with_domain(updated_logins, hakodomain, basedomain));
          Some(updated_text)    
        },
        None => {
          // The existing login is not found
          None
        }
      }
    },
    None => {
      None
    }
  }
}
