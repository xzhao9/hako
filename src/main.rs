#[macro_use]
extern crate rust_embed;

use std::env;
use std::path::Path;
use std::path::PathBuf;

mod hako;

fn main() {
  // feature 1: keygen
  // feature 2: encrypt a file (must exist)
  // option : remove original file
  // feature 3: decrypt a file (must exist)
  // feature 4: create or append to an encrypted file (may or may not exist)
  // DAT daemon: accept signatured requests for operation on a file
  let args: Vec<String> = env::args().collect();
  let mut hako = hako::Hako::create();
  match hako.init() {
    Ok(()) => {
    },
    Err(err) => {
      panic!("Initialization failed! Fatal error: {}", err);
    }
  }
  if args.len() >= 2 {
    match args[1].as_str() {
      "keygen" => {
        hako.keygen();
      },
      "encrypt" => {
        if args.len() >= 3 {
          hako::crypt::encrypt(hako, &args[2]).unwrap();
        }
      },
      "decrypt" => {
        if args.len() >= 3 {
          let secret = match hako.verify_private_key(None) {
            Ok((s, p)) => s,
            Err(err) => {
              println!("{}", err);
              std::process::exit(1);
            }
          };
          let plaintext = hako::crypt::decrypt(&secret, &PathBuf::from(args[2].to_owned())).unwrap();
          let output_filename = format!("{}.ake", args[2]);
          std::fs::write(Path::new(&output_filename), plaintext).unwrap();
        }
      },
      "daemon" => {
        hako::daemon::Daemon::start();
      },
      _ => {
        panic!("Unrecognized command line option: {}", args[1]);
      }
    }
  }
}

// Tests
