// Global data structures
const server = 'http://127.0.0.1:8341';
const maxHierarchy = 5; // Hako supports hierarchy 1-5
// Auto saving
let changed = false;
let lastModifiedTime = null;
// Data
let activeTask = -1;
let taskMeta = [];
let tasks = [[]];

let newItemHtmlPrefix = '<div class="rowwrapper focus" data-level=1> <label class="rowhandle"><input type="checkbox" class="rowchecker"></input></label> <input type="text" class="tasktext" value="';
let newItemHtmlNonfocusPrefix = '<div class="rowwrapper" data-level=1> <label class="rowhandle"><input type="checkbox" class="rowchecker"></input></label> <input type="text" class="tasktext" value="';
let newItemHtmlSuffix = '"></input></div>';

function readCookie(name) {
    let cookie = {};
    document.cookie.split(';').forEach(function(el) {
        let [k,v] = el.split('=');
        cookie[k.trim()] = v;
    })
    return cookie[name];
}

function updateLastModifiedTime() {
    changed = true;
    lastModifiedTime = new Date();
}

function requestTaskFile(filename, title, callback) {
    let xhr = new XMLHttpRequest();
    let data = {name: filename, title: title};
    xhr.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) { // Done and Success
            fillContent(JSON.parse(this.response));
        } else if(this.readyState === 4 && this.status !== 200) { // Done and failed
            alert("Failed to retrieve the active task file: " + filename);
        }
    };
    xhr.open('POST', server + "/loadtask", true);
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(data));
}

function manualCheck(item) {
    item = item.replace("tasktext", "tasktext jobdone");
    item = item.replace('rowchecker">', 'rowchecker" checked>');
    return item;
}

function manualLevel(item, level) {
    item = item.replace("data-level=1", "data-level=" + level);
    return item;
}

function updateTasks() {
    let tasktexts = document.getElementsByClassName("tasktext");
    tasks[activeTask].tasks = []; // reset
    for(let i = 0; i < tasktexts.length; i ++) {
        let text = tasktexts[i].value;
        let todo = !hasClass(tasktexts[i], "jobdone");
        let level = parseInt(tasktexts[i].closest("div.rowwrapper").dataset.level);
        tasks[activeTask].tasks.push({text: text, todo: todo, level: level});
    }
}

function saveTaskFile(callback) {
    let saveind = document.getElementById("savestatus");
    saveind.innerHTML = "saving..."
    saveind.hidden = false;
    let xhr = new XMLHttpRequest();
    updateTasks();
    let data = tasks[activeTask];
    xhr.onreadystatechange = function() {
        if(this.readyState === 4 && this.status === 200) { // success
            changed = false; // already saved.
            setTimeout(function() {
                saveind.innerHTML = "saved.";
                setTimeout(function() {
                    saveind.hidden = true;
                    saveind.innerHTML = "saving...";
                }, 2000);
            }, 300);
            if(callback) {
                callback();
            }
        } else if(this.readyState === 4) { // failed
            saveind.innerHTML = "save failed";
            addClass(saveind, "savefail");
        }
    };
    xhr.open('POST', server + "/savetask");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xhr.send(JSON.stringify(data));
}

function hasClass(element, c) {
    return element.classList.contains(c);
}

function removeClass(element, c) {
    // check if element has class
    if(hasClass(element, c)) {
        element.classList.remove(c);
    } else { // already moved
        return;
    }
}

function addClass(element, c) {
    // check if element has class already
    if(hasClass(element, c)) {
        return;
    } else {
        element.classList.add(c);
    }
}

function isTextEmpty(cb) {
    if(cb.value.length === 0) {
        return true;
    } else {
        return false;
    }
}

// return the subtree
function getRowSet(rowEntry) {
    let ret = [];
    let rowWrapper = rowEntry.closest("div.rowwrapper");
    let rowLevel = parseInt(rowWrapper.dataset.level);
    while(true) {
        let nextTask = getNextEntry(rowEntry);
        if(nextTask === null) {
            break;
        } else {
            let nextWrapper = nextTask.closest("div.rowwrapper");
            let nLevel = parseInt(nextWrapper.dataset.level);
            if(nLevel > rowLevel) {
                ret.push(nextWrapper);
                rowEntry = nextTask;
            } else {
                break;
            }
        }
    }
    return ret;
}

function fillRow(rowWrapper, task) {
    rowWrapper.dataset.level = task.level;
    let taskText = rowWrapper.querySelector(".tasktext");
    taskText.value = task.text;
    let checkbox = rowWrapper.querySelector(".rowchecker");
    if(task.todo) {
        addClass(taskText, "jobdone");
        checkbox.checked = true;
    } else {
        removeClass(taskText, "jobdone");
        checkbox.checked = false;
    }
}

function getTaskFromRow(rowWrapper) {
    let level = rowWrapper.dataset.level;
    let taskText = rowWrapper.querySelector(".tasktext");
    let todo = hasClass(taskText, "jobdone");
    let text = rowWrapper.querySelector(".tasktext").value;
    let task = {level: level, todo: todo, text: text};
    return task;
}

// NOTE: this function requires rowset1 appear before rowset2
function swapRowSet(rowset1, rowset2) {
    // Backup rowset 1 data
    let backupData = []; // each element: level, text, todo
    rowset1.forEach(function(item) {
        backupData.push(getTaskFromRow(item));
    });
    // swap the content
    let j = 0, k = 0;
    let newEntry = null;
    for(let i = 0; i < (rowset1.length + rowset2.length); i ++) {
        let c = null;
        let slot = null;
        if(j >= rowset2.length) { // fill in a rowset1 item
            c = backupData[k];
            k ++;
        } else { // fill in a rowset2 item
            c = getTaskFromRow(rowset2[j]);
            j ++;
        }
        // fill c into the current slot
        if(i >= rowset1.length) {
            slot = rowset2[i - rowset1.length];
        } else {
            slot = rowset1[i];
        }
        // console.log(slot);
        // console.log(c);
        if(j == rowset2.length && k == 1) { // return the new location of rowset1
            newEntry = slot;
        }
        fillRow(slot, c);
    }
    return newEntry;
}

function getNextEntry(cb) {
    let nextRow = cb.closest("div.rowwrapper").nextElementSibling;
    if(nextRow === null) {
        return null;
    } else {
        return nextRow.getElementsByClassName("tasktext")[0];
    }
}

function getPrevEntry(cb) {
    let prevRow = cb.closest("div.rowwrapper").previousElementSibling;
    if(prevRow === null) {
        return null;
    } else {
        return prevRow.getElementsByClassName("tasktext")[0];
    }
}

function getPrevEntrySameLevel(cb) {
    let cur = cb;
    let cbparent = cur.closest("div.rowwrapper");
    let curLevel = cbparent.dataset.level;
    while(true) {
        let prevRow = cbparent.previousElementSibling;
        if(prevRow === null) { // reaches top
            return null;
        } else {
            if(prevRow.dataset.level < curLevel) {
                return null;
            } else if(prevRow.dataset.level > curLevel) { // continue search
                cbparent = prevRow;
            } else { // found the same level
                return prevRow.getElementsByClassName("tasktext")[0];
            }
        }
    }
}

function getNextEntrySameLevel(cb) {
    let cur = cb;
    let cbparent = cur.closest("div.rowwrapper");
    let curLevel = cbparent.dataset.level;
    while(true) {
        let nextRow = cbparent.nextElementSibling;
        if(nextRow === null) { // reaches top
            return null;
        } else {
            if(nextRow.dataset.level < curLevel) {
                return null;
            } else if(nextRow.dataset.level > curLevel) { // continue search
                cbparent = nextRow;
            } else { // found the same level
                return nextRow.getElementsByClassName("tasktext")[0];
            }
        }
    }
}

function getFocusItem() {
    let lines = document.getElementsByClassName("rowwrapper");
    for(let i = 0; i < lines.length; i ++) {
        if(hasClass(lines[i], "focus")) {
            return lines[i];
        }
    }
    return null;
}

function moveFocus(targetEntry) {
    let lines = document.getElementsByClassName("tasktext");
    for(let j = 0; j < lines.length; j ++) {
        removeClass(lines[j].closest("div.rowwrapper"), "focus");
    }
    addClass(targetEntry.closest("div.rowwrapper"), "focus");
    targetEntry.focus();
}

function moveUp(targetEntry) {
    let prevEntry = getPrevEntrySameLevel(targetEntry);
    if(prevEntry !== null) {
        // swap the subtree of the curRow
        let rowSet1 = getRowSet(prevEntry);
        rowSet1.unshift(prevEntry.closest("div.rowwrapper"));
        let rowSet2 = getRowSet(targetEntry);
        rowSet2.unshift(targetEntry.closest("div.rowwrapper"));
        swapRowSet(rowSet1, rowSet2);
        moveFocus(rowSet1[0].querySelector(".tasktext"));
    }
}

function moveDown(targetEntry) {
    let nextEntry = getNextEntrySameLevel(targetEntry);
    if(nextEntry !== null) {
        // swap the subtree of the curRow
        let rowSet1 = getRowSet(targetEntry);
        rowSet1.unshift(targetEntry.closest("div.rowwrapper"));
        let rowSet2 = getRowSet(nextEntry);
        rowSet2.unshift(nextEntry.closest("div.rowwrapper"));
        newEntry = swapRowSet(rowSet1, rowSet2);
        moveFocus(newEntry.querySelector(".tasktext"));
    }
}

function moveLeft(targetEntry) {
    let parent = targetEntry.closest("div.rowwrapper");
    let oldLevel = parseInt(parent.dataset.level);
    if(oldLevel > 1 && oldLevel <= 5) {
        // move its child entries to the left
        let rowSet = getRowSet(targetEntry);
        rowSet.forEach(function(item) {
            item.dataset.level = parseInt(item.dataset.level) - 1;
        });
        parent.dataset.level = oldLevel - 1;
    }
}

function moveRight(targetEntry) {
    let parent = targetEntry.closest("div.rowwrapper");
    let oldLevel = parseInt(parent.dataset.level);
    if(oldLevel > 0 && oldLevel < 5) {
       // move its child entries to the right
        let rowSet = getRowSet(targetEntry);
        rowSet.forEach(function(item) {
            item.dataset.level = parseInt(item.dataset.level) + 1;
        });
        parent.dataset.level = oldLevel + 1; 
    }
}

function insertRowBefore(row) {
    let rowWrapper = row.closest("div.rowwrapper");
    let newItemHtml = newItemHtmlPrefix + newItemHtmlSuffix;
    rowWrapper.insertAdjacentHTML('beforebegin', newItemHtml);
    let prevEntry = getPrevEntry(row);
    if(prevEntry !== null) { // shouldn't be null here
        addElementKBAction(prevEntry);
        let prevParent = prevEntry.closest("div.rowwrapper");
        prevParent.dataset.level = rowWrapper.dataset.level;
        let checker = prevParent.querySelector("input.rowchecker");
        addCheckBoxAction(checker);
        addFocusAction(prevEntry);
        moveFocus(row);  // Don't move the focus when adding before current row
    }
}

function insertRow(rowBefore, pos) {
    let rowWrapperBefore = rowBefore.closest("div.rowwrapper");
    let newItemHtml = newItemHtmlPrefix + newItemHtmlSuffix;
    rowWrapperBefore.insertAdjacentHTML('afterend', newItemHtml);
    let nextEntry = getNextEntry(rowBefore);
    let firstPart = "";
    let secondPart = "";
    if(pos !== -1) {
        firstPart = rowBefore.value.substr(0, pos);
        secondPart = rowBefore.value.substr(pos);
    }
    if(nextEntry !== null) { // shouldn't be null here.
        addElementKBAction(nextEntry);
        let nextParent = nextEntry.closest("div.rowwrapper");
        let checker = nextParent.querySelector("input.rowchecker");
        nextParent.dataset.level = rowWrapperBefore.dataset.level;
        addCheckBoxAction(checker);
        addFocusAction(nextEntry);
        moveFocus(nextEntry);
        if(pos !== -1) {
            rowBefore.value = firstPart;
            nextEntry.value = secondPart;
            // move to the beginning of the next entry
            nextEntry.selectionStart = nextEntry.selectionEnd = 0;
        }
    }
}

// Delete current row and append the value to the previous row
function deleteRow(targetRow) {
    let prevEntry = getPrevEntry(targetRow);
    let val = targetRow.value;
    if(prevEntry !== null) {
        targetRow.closest("div.rowwrapper").remove();
        moveFocus(prevEntry);
        let pval = prevEntry.value;
        prevEntry.value = pval + val;
        // move cursor to the end of previous row
        prevEntry.selectionStart = prevEntry.selectionEnd = pval.length;
    }
}

// Add keyboard navigation actions to an element
function addElementKBAction(txt) {
    txt.addEventListener("keydown", event => {
        if(event.ctrlKey && event.keyCode === 38) { // 'Ctrl+Up'
            updateLastModifiedTime();
            moveUp(txt);
        } else if(event.ctrlKey && event.keyCode === 40) { // 'Ctrl+Down'
            updateLastModifiedTime();
            moveDown(txt);
        } else if(event.keyCode === 40) { // 'Down'
            // get the down neighbor of txt, could be empty
            let nextEntry = getNextEntry(txt);
            if(nextEntry !== null) { // Move focus to the next entry
                moveFocus(nextEntry);
            }
        } else if(event.keyCode === 38) { // 'Up'
            // get the up neighbor of txt, could be empty
            let prevEntry = getPrevEntry(txt);
            if(prevEntry !== null) { // Move focus to the previous entry
                moveFocus(prevEntry);
            }
        } else if(event.keyCode === 13) { // 'Enter'
            // create a new entry and bind the events
            updateLastModifiedTime();
            // if the cursor is at the beginning
            let len = txt.value.length;
            let pos = event.target.selectionEnd;
            if(pos === 0 && len !== 0) {
                insertRowBefore(txt);
            } else {
                insertRow(txt, pos);
            }
        } else if(event.keyCode === 8) { // 'BackSpace'
            updateLastModifiedTime();
            let pos = event.target.selectionStart;
            let end = event.target.selectionEnd;
            if(pos !== end) { // normal remove;
                return;
            }
            if(pos === 0) { // if at the beginning of the line
                deleteRow(txt);
                event.preventDefault(); // Don't delete the last char on the previous row
            }
        } else if(event.shiftKey && event.keyCode === 9) { // 'Shift + Tab'
            updateLastModifiedTime();
            moveLeft(txt);
            event.preventDefault();
        } else if(event.keyCode === 9 || event.which === 9) { // 'Tab'
            updateLastModifiedTime();
            moveRight(txt);
            event.preventDefault();
        } else if (event.ctrlKey && event.keyCode === 37) { // 'Ctrl + Left'
            updateLastModifiedTime();
            moveLeft(txt);
            event.preventDefault();
        } else if (event.ctrlKey && event.keyCode === 39) { // 'Ctrl + Right'
            updateLastModifiedTime();
            moveRight(txt);
            event.preventDefault();
        } else if(event.keyCode === 37) { // 'Left'
            // do nothing
        } else if(event.keyCode === 39) { // 'Right'
            // do nothing
        } else if(event.keyCode === 229 || event.keyCode === 27) { // 'Special Char Prelude', 'Esc'
            // do nothing
        } else if(event.keyCode === 35) { // 'Home'
            // do nothing
        } else if(event.keyCode === 36) { // 'End'
            // do nothing
        } else if(event.keyCode <= 123 && event.keyCode >= 112) { // 'F1 - F12'
            // do nothing
        } else if(event.keyCode === 46) { // 'Delete'
            updateLastModifiedTime();
            let pos = event.target.selectionStart;
            if(pos !== event.target.selectionEnd) { // normal delete
                return;
            }
            if(pos === txt.value.length) { // End of the line
                event.preventDefault();
                let nextEntry = getNextEntry(txt);
                if(nextEntry !== null) { // remove the next entry and put its text on the current row
                    let val = nextEntry.value;
                    deleteRow(nextEntry);
                    txt.selectionStart = txt.selectionEnd = pos;
                }
            }
        } else { // anything else
            updateLastModifiedTime();
        }
    });
}

// Add actions to checkboxes
function addCheckBoxAction(cb) {
    let taskText = cb.closest("div.rowwrapper").getElementsByClassName("tasktext")[0];
    if(cb.checked) {
        addClass(taskText, "jobdone");
    } else {
        removeClass(taskText, "jobdone");
    }
    cb.onclick = function() {
        if(cb.checked) {
            updateLastModifiedTime();
            addClass(taskText, "jobdone");
        } else {
            updateLastModifiedTime();
            removeClass(taskText, "jobdone");
        }
    }
}

function loadTask(index) {
    activeTask = index;
    // update the cookie
    document.cookie = "activeTask=" + activeTask;
    let head = document.getElementById("titletop");
    head.innerHTML = taskMeta.tasks[activeTask].title;
    requestTaskFile(taskMeta.tasks[activeTask].name,
                    taskMeta.tasks[activeTask].title, fillContent);
}

function addSideBarClickAction(sidebar) {
    sidebar.onclick = function() {
        let index = parseInt(sidebar.dataset.index);
        if(taskMeta.tasks.length > index) {
            // save current task, then load the next one
            if(changed){
                saveTaskFile(function() {
                    loadTask(index);
                });
            } else {
                loadTask(index);
            }
        }
    }
}

function addFocusAction(cb) {
    cb.onclick = function() {
        moveFocus(cb);
    };
}

function checkBoxAction() {
    let boxes = document.getElementsByClassName("rowchecker");
    for(let i = 0; i < boxes.length; i ++) {
        let item = boxes[i];
        addCheckBoxAction(item);
    }
}

function focusAction() {
    let textLines = document.getElementsByClassName("tasktext");
    for(let i = 0; i < textLines.length; i ++) {
        let item = textLines[i];
        addFocusAction(item);
    }
}

function textKBAction() {
    let textLines = document.getElementsByClassName("tasktext");
    for(let i = 0; i < textLines.length; i ++) {
        addElementKBAction(textLines[i]);
    }
}

function initButtons() {
    let newList = document.getElementById("newlist");
    let newItem = document.getElementById("newitem");
    let upBtn = document.getElementById("moveup");
    let downBtn = document.getElementById("movedown");
    let leftBtn = document.getElementById("moveleft");
    let rightBtn = document.getElementById("moveright");
    
    // newItem Button
    newItem.onclick = function() {
        let cur = getFocusItem();
        if(cur !== null) {
            insertRow(cur, -1);
        }
    };

    // MoveUp Button
    upBtn.onclick = function() {
        let cur = getFocusItem();
        if(cur !== null) {
            moveUp(cur); 
        }
    };

    // MoveDown Button
    downBtn.onclick = function() {
        let cur = getFocusItem();
        if(cur !== null) {
            moveDown(cur);
        }
    };

    // MoveLeft Button
    leftBtn.onclick = function() {
        let cur = getFocusItem();
        if(cur !== null) {
            moveLeft(cur);
        }
    }

    // Moveright Button
    rightBtn.onclick = function() {
        let cur = getFocusItem();
        if(cur !== null) {
            moveRight(cur);
        }
    }
}

// fill in the sidebar and title 
function fillSidebar() {
    // Remove the old sidebar content
    let sidebarTitles = document.querySelectorAll(".downleftbar > .listtitle");
    for(let i = 0; i < sidebarTitles.length; i ++) {
        sidebarTitles[i].remove();
    }
    // Fill in the new ones
    let sidebar = document.querySelector(".downleftbar");
    for(let i = 0; i < taskMeta.tasks.length; i ++) {
        let tlist = '<div class="listtitle" data-index="' + i + '"> <a>' + taskMeta.tasks[i].title + '</a> </div>';
        sidebar.insertAdjacentHTML('beforeend', tlist);
    }
    let sidebarTasks = document.getElementsByClassName("listtitle");
    for(let i = 0; i < sidebarTasks.length; i ++) {
        addSideBarClickAction(sidebarTasks[i]);
    }
}

function finishInit() {
    checkBoxAction();
    focusAction();
    textKBAction();
    initButtons();
}

function fillContent(contents) {
    tasks[activeTask] = contents;
    // remove all old contents
    let rowwrappers = document.querySelectorAll(".taskrows > .rowwrapper");
    for(let i = 0; i < rowwrappers.length; i ++) {
        rowwrappers[i].remove();
    }
    // fill in new contents
    let taskrow = document.querySelector(".taskrows");
    if(contents.tasks.length === 0) {
        let item = newItemHtmlPrefix + newItemHtmlSuffix;
        taskrow.insertAdjacentHTML('beforeend', item);
    } else {
        for(let i = 0; i < contents.tasks.length; i ++) {
            let item = "";
            if(i === 0) {
                item = newItemHtmlPrefix + contents.tasks[i].text + newItemHtmlSuffix;
            } else {
                item = newItemHtmlNonfocusPrefix + contents.tasks[i].text + newItemHtmlSuffix;
            }
            if(!contents.tasks[i].todo) { // if done, check the mark
                item = manualCheck(item);
            }
            if(contents.tasks[i].level) {
                item = manualLevel(item, contents.tasks[i].level); 
            }
            taskrow.insertAdjacentHTML('beforeend', item);
        }
    }
    // Finally, bind all actions
    finishInit();
}

function init(tlist) {
    taskMeta = tlist;
    if(taskMeta.length !== 0) { // if something real is being read
        fillSidebar();
        activeTask = 0;
        let task = readCookie("activeTask");
        if(task !== undefined && task !== "NaN") {
            activeTask = parseInt(task);
        }
        loadTask(activeTask);
    }
}

function requestTaskLists(callback) {
    let xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function() {
        // fill in the global data structures
        if(this.readyState === 4 && this.status === 200) { // Done and success
            callback(JSON.parse(this.response));
        } else if(this.readyState === 4 && this.status !== 200) { // Done but failed
            alert("Failed to retrieve task lists.");
        }
    };
    xhr.open('GET', server + "/listtasks");
    xhr.send();
}

function main() {
    requestTaskLists(init);
    // Auto-save in every 4 seconds
    setInterval(function() {
        if(changed) {
            let now = new Date();
            let timeToLastModified = now.getTime() - lastModifiedTime.getTime();
            if(timeToLastModified >= 2000) { // nothing changed in the last 2 seconds
                saveTaskFile();
            }
        }
    }, 4000);
    document.addEventListener("keydown", function(e) {
        if (e.ctrlKey && e.keyCode == 83) {
            e.preventDefault();
            // Save the document to the backend;
            saveTaskFile();
        }
    });
}

main();
